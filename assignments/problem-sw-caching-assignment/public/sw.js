var CACHE_STATIC_NAME = 'static-v1';
var CACHE_DYNAMIC_NAME = 'dynamic-v2';

// 2) Identify the AppShell (i.e. core assets your app requires to provide its basic "frame")
self.addEventListener('install', (event)=>{
    console.log('[SW] Installing ...');
    event.waitUntil(
        caches.open(CACHE_STATIC_NAME)
            .then((cache)=>{
                console.log('adding cache!');
                cache.addAll([
                    '/',
                    '/index.html',
                    '/dynamic',
                    '/dynamic/index.html',
                    '/src/js/main.js',
                    '/src/js/promise.js',
                    '/src/js/fetch.js',
                    '/src/js/material.min.js',
                    '/src/css/app.css',
                    'https://fonts.googleapis.com/css?family=Roboto:400,700',
                    'https://fonts.googleapis.com/icon?family=Material+Icons',
                    'https://cdnjs.cloudflare.com/ajax/libs/material-design-lite/1.3.0/material.indigo-pink.min.css'
                 
                ]);
            })
    )
});

self.addEventListener('activate', (event)=>{
    console.log('[SW] activating ...');
    event.waitUntil(
        caches.keys()
            .then((keyList)=>{
                return Promise.all(keyList.map((key)=>{
                    if(key != CACHE_DYNAMIC_NAME && key != CACHE_STATIC_NAME){
                        console.log('cleaning old cache');
                        return caches.delete(key);
                    }
                }));
            })
    );

});

self.addEventListener('fetch', function(event) {
    event.respondWith(
      caches.match(event.request)
        .then((response)=>{
          if (response) {
            return response;
          } else {
            return fetch(event.request).then((res)=>{
              return caches.open(CACHE_DYNAMIC_NAME).then((cache)=>{
                cache.put(event.request.url, res.clone());
                return res;
              }).catch(()=>{
  
              });
            });
          }
        })
    );
  });
// 3) Precache the AppShell
// 4) Add Code to fetch the precached assets from cache when needed
// 5) Precache other assets required to make the root index.html file work
// 6) Change some styling in the main.css file and make sure that the new file gets loaded + cached (hint: versioning)
// 7) Make sure to clean up unused caches
// 8) Add dynamic caching (with versioning) to cache everything in your app when visited/ fetched by the user

// Important: Clear your Application Storage first to get rid of the old SW & Cache from the Main Course Project!'